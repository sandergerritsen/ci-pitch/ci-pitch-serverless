# CI Pitch Serverless


## How-to setup this project

### Google Cloud

#### Setup

- Create a new Google Cloud `project`, name: "ci-pitch".
- Create a `Service account`, name: "pipeline".
![GCP IAM Service Account](doc/img/gcp-iam-serviceaccount.png)
  - Add roles:
    - Service Account User
    - Storage Admin
    - Cloud Run Service Agent
    - Cloud Build Service Account


- Create a storage bucket, name: "ci-pitch_cloudbuild"
![GCP Storage Bucket for Cloud Build logs](doc/img/bucket_cloudbuild.png)

- Navigate to the `Cloud Build` module. At first try, you are asked to enable the module. Click `ENABLE` to continue.
![Enable Cloud Build module](doc/img/cloudbuild_enable.png)

### Gitlab

#### CI/CD Variables
The gitlab pipeline, available in gitlab-ci.yml, needs a few configuration variables. Navigate to Settings -> CI/CD -> Variables and add these variables:

- PRODUCTION_URL : [your cloud run endpoint url]
- PROJECT_NAME   : "CI Pitch Serverless"
- SLACK_WEBHOOK  : [create slack web hook and copy the url here]
- GCLOUD_REGION  : "europe-west1"
- GCLOUD_CLOUDBUILD_BUCKET : [create a bucket and copy gs: link]
- GCLOUD_SERVICE_ACCOUNT   : [create a GCP service account and copy name here]"
- GCLOUD_SERVICE_ACCOUNT_KEY : [create a JSON key for the service account and past content here]
- DOCKER_REGISTRY_IMAGE : "eu.gcr.io/[google cloud project id]/serverless"
- GCLOUD_PROJECT : [your google cloud project ID]

![Gitlab CI variables](doc/img/gitlab-ci-variables.png)

### Commit to master branch

The gitlab CI pipeline is set to act on every commit to the `master` branch. Now that everything is set up, you are free to use the automated deployment to serverless back-end. Push a commit or use a merge-request to get your new code to the `master` branch. You will notice that a new pipeline has started and your app will have been updated as soon as the pipeline is finished.  

![Cloud Run Instance](doc/img/cloud-run-instance.png)

## Extra information

### Docker

#### Build
- `docker build --target=base -t base .` to build development image
- `docker build --target=release -t release .` to build production image

#### Run
- `docker-compose up` to fire up all services


### Google Cloud

#### Build image
- `gcloud builds submit --tag=eu.gcr.io/ci-pitch/serverless:release123`

#### Deploy to Cloud Run
- `gcloud beta run deploy ci-pitch-serverless --image eu.gcr.io/ci-pitch/serverless:release123 --platform managed --quiet`
  - --quiet : don't ask any questions
  - --platform managed : default Cloud Run deployment
  - --image xxx : specify image to deploy

##### Web server listening port

GCP Cloud Run required the container to respond to requests at port 8080. The default php-apache image listens to port 80. The Dockerfile contains a `sed` line that changes 80 to 8080.

