FROM php:7.3-apache AS base

RUN sed -i 's/80/8080/g' /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf

FROM base AS release

COPY public/ /var/www/html/

RUN chown -R www-data:www-data /var/www/html
